/*
 *
 * This file is part of Mastik.
 *
 * Mastik is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mastik is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mastik.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <fr.h>
#include <util.h>
#include <symbol.h>

int main(int ac, char **av) {

 if(ac != 3){
   printf("Usage: %s <libc with symbols> <function to trace>\n", av[0]);
   exit(1);
 }

  uint64_t offset = sym_getsymboloffset(av[1], av[2]);
  
  if(offset != -1){
    printf("Tracing calls to %s (%#08lx)\n", av[2], offset);
  }else{
    exit(1);
  }

  void *ptr = map_offset("/lib/x86_64-linux-gnu/libc-2.28.so", offset);

  if (ptr == NULL)
    exit(0);

  fr_t fr = fr_prepare();
  fr_monitor(fr, ptr);

  uint16_t res[1];
  fr_probe(fr, res);

  int lines=0;
  for (;;) {
    fr_probe(fr, res);
    if (res[0] < 100)
      printf("%4d: %s %s\n", ++lines, "Call to", av[2]);
    delayloop(10000);
  }
}

